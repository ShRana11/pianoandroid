package com.example.androidparticlestarter;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity {
    // MARK: Debug info
    private final String TAG = "jenelle";
    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "shrana2890@gmail.com";
    private final String PARTICLE_PASSWORD = "Shranarana48";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "44002c000f47363333343437";

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    private ParticleDevice mDevice;
    String node = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());

        // 2. Setup your device variable
        getDeviceFromCloud();
       // change();




    }


    /**
     * Custom function to connect to the Particle Cloud and get the device
     */
    public void getDeviceFromCloud() {
        // This function runs in the background
        // It tries to connect to the Particle Cloud and get your device
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);
                mDevice = particleCloud.getDevice(DEVICE_ID);
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    public void b1(View view){
        node = "B6,8,B6";
        change();

    }
    public void b2(View view){
        node = "G6,8,G6";
        change();

    }
    public void b3(View view){
        node = "F6,8,F6";
        change();

    }
    public void b4(View view){
        node = "E6,8,E6";
        change();

    }
    public void b5(View view){
        node = "D6,8,D6";
        change();

    }
    public void b6(View view){
        node = "C6,8,C6";
        change();

    }
    public void b7(View view){
        node = "B6,8,B6";
        change();

    }
    public void b8(View view){
        node = "G5,8,G5";
        change();

    }
    public void b9(View view){
        node = "F5,8,F5";
        change();

    }
    public void b10(View view){
        node = "E5,8,E5";
        change();

    }
    public void b11(View view){
        node = "D5,8,D5";
        change();

    }
    public void b12(View view){
        node = "C5,8,C5";
        change();

    }

    public void change() {
        Log.d("JENELLE", "change done;");


        // logic goes here

//        // 1. get the r,g,b value from the UI
//        EditText red = (EditText) findViewById(R.id.red);
//        EditText green = (EditText) findViewById(R.id.green);
//        EditText blue = (EditText) findViewById(R.id.blue);

//        // Convert these to strings
//        String r = "sred.getText().toString()";
//        String g = "green.getText().toString()";
//        String b = "blue.getText().toString()";
//
//        Log.d(TAG, "Red: " + r);
//        Log.d(TAG, "Green: " + g);
//        Log.d(TAG, "Blue: " + b);


        String commandToSend = node;
        Log.d(TAG, "Command to send to particle: " + commandToSend);


        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {

                // 2. build a list and put the r,g,b into the list
                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add(commandToSend);

                // 3. send the command to the particle
                try {
                    mDevice.callFunction("music", functionParameters);
                } catch (ParticleDevice.FunctionDoesNotExistException e) {
                    e.printStackTrace();
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Sent colors command to device.");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }
}
